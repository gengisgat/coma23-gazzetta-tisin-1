<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaEditorialBoardPivotTable extends Migration
{
    public function up()
    {
        Schema::create('area_editorial_board', function (Blueprint $table) {
            $table->unsignedBigInteger('editorial_board_id');
            $table->foreign('editorial_board_id', 'editorial_board_id_fk_8939366')->references('id')->on('editorial_boards')->onDelete('cascade');
            $table->unsignedBigInteger('area_id');
            $table->foreign('area_id', 'area_id_fk_8939366')->references('id')->on('areas')->onDelete('cascade');
        });
    }
}
