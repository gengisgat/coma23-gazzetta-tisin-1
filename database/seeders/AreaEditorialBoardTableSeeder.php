<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AreaEditorialBoardTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('area_editorial_board')->delete();
        
        \DB::table('area_editorial_board')->insert(array (
            0 => 
            array (
                'editorial_board_id' => 1,
                'area_id' => 2,
            ),
            1 => 
            array (
                'editorial_board_id' => 1,
                'area_id' => 5,
            ),
            2 => 
            array (
                'editorial_board_id' => 1,
                'area_id' => 6,
            ),
            3 => 
            array (
                'editorial_board_id' => 2,
                'area_id' => 1,
            ),
            4 => 
            array (
                'editorial_board_id' => 3,
                'area_id' => 3,
            ),
            5 => 
            array (
                'editorial_board_id' => 3,
                'area_id' => 4,
            ),
        ));
        
        
    }
}