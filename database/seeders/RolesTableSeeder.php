<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Admin',
                'created_at' => '2023-09-05 15:02:02',
                'updated_at' => '2023-09-05 15:02:03',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Direttore',
                'created_at' => '2023-09-05 15:02:01',
                'updated_at' => '2023-09-01 08:27:40',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Giornalista',
                'created_at' => '2023-09-01 08:27:52',
                'updated_at' => '2023-09-01 08:27:52',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}