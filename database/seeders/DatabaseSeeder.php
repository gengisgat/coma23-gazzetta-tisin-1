<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(AreasTableSeeder::class);
        $this->call(EditorialBoardsTableSeeder::class);
        $this->call(AreaEditorialBoardTableSeeder::class);
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
        ]);
        $this->call(ArticlesTableSeeder::class);
    }
}
