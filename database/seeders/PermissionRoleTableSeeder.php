<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'role_id' => 1,
                'permission_id' => 1,
            ),
            1 => 
            array (
                'role_id' => 1,
                'permission_id' => 2,
            ),
            2 => 
            array (
                'role_id' => 1,
                'permission_id' => 3,
            ),
            3 => 
            array (
                'role_id' => 1,
                'permission_id' => 4,
            ),
            4 => 
            array (
                'role_id' => 1,
                'permission_id' => 5,
            ),
            5 => 
            array (
                'role_id' => 1,
                'permission_id' => 6,
            ),
            6 => 
            array (
                'role_id' => 1,
                'permission_id' => 7,
            ),
            7 => 
            array (
                'role_id' => 1,
                'permission_id' => 8,
            ),
            8 => 
            array (
                'role_id' => 1,
                'permission_id' => 9,
            ),
            9 => 
            array (
                'role_id' => 1,
                'permission_id' => 10,
            ),
            10 => 
            array (
                'role_id' => 1,
                'permission_id' => 11,
            ),
            11 => 
            array (
                'role_id' => 1,
                'permission_id' => 12,
            ),
            12 => 
            array (
                'role_id' => 1,
                'permission_id' => 13,
            ),
            13 => 
            array (
                'role_id' => 1,
                'permission_id' => 14,
            ),
            14 => 
            array (
                'role_id' => 1,
                'permission_id' => 15,
            ),
            15 => 
            array (
                'role_id' => 1,
                'permission_id' => 16,
            ),
            16 => 
            array (
                'role_id' => 1,
                'permission_id' => 17,
            ),
            17 => 
            array (
                'role_id' => 1,
                'permission_id' => 18,
            ),
            18 => 
            array (
                'role_id' => 1,
                'permission_id' => 19,
            ),
            19 => 
            array (
                'role_id' => 1,
                'permission_id' => 20,
            ),
            20 => 
            array (
                'role_id' => 1,
                'permission_id' => 21,
            ),
            21 => 
            array (
                'role_id' => 1,
                'permission_id' => 22,
            ),
            22 => 
            array (
                'role_id' => 1,
                'permission_id' => 23,
            ),
            23 => 
            array (
                'role_id' => 1,
                'permission_id' => 24,
            ),
            24 => 
            array (
                'role_id' => 1,
                'permission_id' => 25,
            ),
            25 => 
            array (
                'role_id' => 1,
                'permission_id' => 26,
            ),
            26 => 
            array (
                'role_id' => 1,
                'permission_id' => 27,
            ),
            27 => 
            array (
                'role_id' => 1,
                'permission_id' => 28,
            ),
            28 => 
            array (
                'role_id' => 1,
                'permission_id' => 29,
            ),
            29 => 
            array (
                'role_id' => 1,
                'permission_id' => 30,
            ),
            30 => 
            array (
                'role_id' => 1,
                'permission_id' => 31,
            ),
            31 => 
            array (
                'role_id' => 1,
                'permission_id' => 32,
            ),
            32 => 
            array (
                'role_id' => 1,
                'permission_id' => 33,
            ),
            33 => 
            array (
                'role_id' => 2,
                'permission_id' => 17,
            ),
            34 => 
            array (
                'role_id' => 2,
                'permission_id' => 18,
            ),
            35 => 
            array (
                'role_id' => 2,
                'permission_id' => 19,
            ),
            36 => 
            array (
                'role_id' => 2,
                'permission_id' => 20,
            ),
            37 => 
            array (
                'role_id' => 2,
                'permission_id' => 21,
            ),
            38 => 
            array (
                'role_id' => 2,
                'permission_id' => 22,
            ),
            39 => 
            array (
                'role_id' => 2,
                'permission_id' => 23,
            ),
            40 => 
            array (
                'role_id' => 2,
                'permission_id' => 24,
            ),
            41 => 
            array (
                'role_id' => 2,
                'permission_id' => 25,
            ),
            42 => 
            array (
                'role_id' => 2,
                'permission_id' => 26,
            ),
            43 => 
            array (
                'role_id' => 2,
                'permission_id' => 27,
            ),
            44 => 
            array (
                'role_id' => 2,
                'permission_id' => 28,
            ),
            45 => 
            array (
                'role_id' => 2,
                'permission_id' => 29,
            ),
            46 => 
            array (
                'role_id' => 2,
                'permission_id' => 30,
            ),
            47 => 
            array (
                'role_id' => 2,
                'permission_id' => 31,
            ),
            48 => 
            array (
                'role_id' => 2,
                'permission_id' => 32,
            ),
            49 => 
            array (
                'role_id' => 2,
                'permission_id' => 33,
            ),
            50 => 
            array (
                'role_id' => 2,
                'permission_id' => 1,
            ),
            51 => 
            array (
                'role_id' => 2,
                'permission_id' => 2,
            ),
            52 => 
            array (
                'role_id' => 2,
                'permission_id' => 3,
            ),
            53 => 
            array (
                'role_id' => 2,
                'permission_id' => 4,
            ),
            54 => 
            array (
                'role_id' => 2,
                'permission_id' => 5,
            ),
            55 => 
            array (
                'role_id' => 2,
                'permission_id' => 6,
            ),
            56 => 
            array (
                'role_id' => 2,
                'permission_id' => 7,
            ),
            57 => 
            array (
                'role_id' => 2,
                'permission_id' => 8,
            ),
            58 => 
            array (
                'role_id' => 2,
                'permission_id' => 9,
            ),
            59 => 
            array (
                'role_id' => 2,
                'permission_id' => 10,
            ),
            60 => 
            array (
                'role_id' => 2,
                'permission_id' => 11,
            ),
            61 => 
            array (
                'role_id' => 2,
                'permission_id' => 12,
            ),
            62 => 
            array (
                'role_id' => 2,
                'permission_id' => 13,
            ),
            63 => 
            array (
                'role_id' => 2,
                'permission_id' => 14,
            ),
            64 => 
            array (
                'role_id' => 2,
                'permission_id' => 15,
            ),
            65 => 
            array (
                'role_id' => 2,
                'permission_id' => 16,
            ),
            66 => 
            array (
                'role_id' => 3,
                'permission_id' => 1,
            ),
            67 => 
            array (
                'role_id' => 3,
                'permission_id' => 2,
            ),
            68 => 
            array (
                'role_id' => 3,
                'permission_id' => 3,
            ),
            69 => 
            array (
                'role_id' => 3,
                'permission_id' => 4,
            ),
            70 => 
            array (
                'role_id' => 3,
                'permission_id' => 5,
            ),
            71 => 
            array (
                'role_id' => 3,
                'permission_id' => 6,
            ),
            72 => 
            array (
                'role_id' => 3,
                'permission_id' => 7,
            ),
            73 => 
            array (
                'role_id' => 3,
                'permission_id' => 8,
            ),
            74 => 
            array (
                'role_id' => 3,
                'permission_id' => 9,
            ),
            75 => 
            array (
                'role_id' => 3,
                'permission_id' => 10,
            ),
            76 => 
            array (
                'role_id' => 3,
                'permission_id' => 11,
            ),
            77 => 
            array (
                'role_id' => 3,
                'permission_id' => 12,
            ),
            78 => 
            array (
                'role_id' => 3,
                'permission_id' => 13,
            ),
            79 => 
            array (
                'role_id' => 3,
                'permission_id' => 14,
            ),
            80 => 
            array (
                'role_id' => 3,
                'permission_id' => 15,
            ),
            81 => 
            array (
                'role_id' => 3,
                'permission_id' => 16,
            ),
            82 => 
            array (
                'role_id' => 3,
                'permission_id' => 17,
            ),
            83 => 
            array (
                'role_id' => 3,
                'permission_id' => 18,
            ),
            84 => 
            array (
                'role_id' => 3,
                'permission_id' => 19,
            ),
            85 => 
            array (
                'role_id' => 3,
                'permission_id' => 20,
            ),
            86 => 
            array (
                'role_id' => 3,
                'permission_id' => 21,
            ),
            87 => 
            array (
                'role_id' => 3,
                'permission_id' => 22,
            ),
            88 => 
            array (
                'role_id' => 3,
                'permission_id' => 23,
            ),
            89 => 
            array (
                'role_id' => 3,
                'permission_id' => 24,
            ),
            90 => 
            array (
                'role_id' => 3,
                'permission_id' => 25,
            ),
            91 => 
            array (
                'role_id' => 3,
                'permission_id' => 26,
            ),
            92 => 
            array (
                'role_id' => 3,
                'permission_id' => 27,
            ),
            93 => 
            array (
                'role_id' => 3,
                'permission_id' => 28,
            ),
            94 => 
            array (
                'role_id' => 3,
                'permission_id' => 29,
            ),
            95 => 
            array (
                'role_id' => 3,
                'permission_id' => 30,
            ),
            96 => 
            array (
                'role_id' => 3,
                'permission_id' => 31,
            ),
            97 => 
            array (
                'role_id' => 3,
                'permission_id' => 32,
            ),
            98 => 
            array (
                'role_id' => 3,
                'permission_id' => 33,
            ),
        ));
        
        
    }
}