<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EditorialBoardsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('editorial_boards')->delete();
        
        \DB::table('editorial_boards')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cultura',
                'created_at' => '2023-08-30 12:40:47',
                'updated_at' => '2023-08-30 12:40:47',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Sport',
                'created_at' => '2023-08-30 12:40:57',
                'updated_at' => '2023-08-30 12:40:57',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cronaca',
                'created_at' => '2023-08-30 12:41:14',
                'updated_at' => '2023-08-30 12:41:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}