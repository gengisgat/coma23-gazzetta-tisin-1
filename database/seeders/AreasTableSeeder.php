<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('areas')->delete();
        
        \DB::table('areas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sport',
                'created_at' => '2023-08-30 12:39:00',
                'updated_at' => '2023-08-30 12:39:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Spettacolo',
                'created_at' => '2023-08-30 12:39:12',
                'updated_at' => '2023-08-30 12:39:12',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cronaca locale',
                'created_at' => '2023-08-30 12:39:30',
                'updated_at' => '2023-08-30 12:39:30',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Cronaca intenazionale',
                'created_at' => '2023-08-30 12:39:41',
                'updated_at' => '2023-08-30 12:39:41',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Cinema',
                'created_at' => '2023-08-30 12:40:15',
                'updated_at' => '2023-08-30 12:40:15',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Teatro',
                'created_at' => '2023-08-30 12:40:23',
                'updated_at' => '2023-08-30 12:40:23',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}