<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$DVkl.k51EYePKjVIs56YueJd/CJgDGJbxfDwURFYqAJrWOUgCpyee',
                'remember_token' => NULL,
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'birth_date' => '2023-09-01',
                'created_at' => NULL,
                'updated_at' => '2023-09-01 08:25:41',
                'deleted_at' => NULL,
                'editorial_board_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'topo.lino',
                'email' => 'topo.lino@tisin.ch',
                'email_verified_at' => NULL,
                'password' => '$2y$10$a4PtDSbAejN8sCr8lqvllebMZuvG0sp96oXCUnznFX3XWIaWgKwZ6',
                'remember_token' => NULL,
                'first_name' => 'Lino',
                'last_name' => 'Topo',
                'birth_date' => '2000-07-13',
                'created_at' => '2023-09-01 08:26:59',
                'updated_at' => '2023-09-05 12:04:09',
                'deleted_at' => NULL,
                'editorial_board_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'wolve.rino',
                'email' => 'wolve.rino@tisin.ch',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Mr9OxOfuOP5jGu8NMhDS/OFoL6JiQiDGiyZaVnkkLE8nHhmrNvB7W',
                'remember_token' => NULL,
                'first_name' => 'Rino',
                'last_name' => 'Wolve',
                'birth_date' => '2020-09-02',
                'created_at' => '2023-09-01 08:29:20',
                'updated_at' => '2023-09-01 08:38:12',
                'deleted_at' => NULL,
                'editorial_board_id' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'emon.dora',
                'email' => 'emon.dora@tisin.ch',
                'email_verified_at' => NULL,
                'password' => '$2y$10$MkAVWGDSdaiVlyGkqIXi9.SVZvy/R3GKa9BuM4L10wbDbyoKfkpve',
                'remember_token' => NULL,
                'first_name' => 'Dora',
                'last_name' => 'Emon',
                'birth_date' => '2020-09-20',
                'created_at' => '2023-09-01 08:32:20',
                'updated_at' => '2023-09-05 12:07:47',
                'deleted_at' => NULL,
                'editorial_board_id' => 3,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'lady.oscar',
                'email' => 'lady.oscar@tisin.ch',
                'email_verified_at' => NULL,
                'password' => '$2y$10$VTKMprm.Hj3eJMaejT/n/OH/7OZVwkhuWLWzKnaruMrZJywbquwc6',
                'remember_token' => NULL,
                'first_name' => 'Oscar',
                'last_name' => 'Lady',
                'birth_date' => '2000-01-19',
                'created_at' => '2023-09-01 08:58:57',
                'updated_at' => '2023-09-01 08:59:25',
                'deleted_at' => NULL,
                'editorial_board_id' => 1,
            ),
        ));
        
        
    }
}