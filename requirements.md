# Requisiti aggiunti

## Dashboard
Inserire dei link utili nel home (creazione nuovo articolo)

vedere l'ultimo articolo scritto

Lista ultimi articoli per il direttore 

Annunci interni e todo-list (per l'implementazione serve una nuova tabella)

## Requisiti utenti
Gestire la creazione dell'articolo con il giornalista loggato

Il giornalista non deve cancellare e modificare gli articoli altrui

Il giornalista deve poter fare la review di articoli altrui

Il giornalista che crea l'articolo deve essere valorizzato direttamente

Lista utenti loggati

Mettere il nome utente/ruolo nel menu in alto

Gestire l'assegnazione dei giornalisti alla redazione in modo più semplice ed evitare che possa modificare altri dati dell'utente

Gestire utente disabilitato e abilitato

Dettaglio sui permessi?


## Requisiti articoli
Link da vedi articoli a modifica articoli

Visualizzare data creazione articolo

Gli articoli andrebbero assgnati a redazioni/aree tematiche

Gestire le bozze degli articoli e permettere al giornalista di individuare i suoi articoli

Gestire stato dell'articolo (pubblicato, bozza, ..) <= implica una modifica del db

Togliere mail da lista articoli


## Gestione pagina password
Se l'utente non è admin, non deve poter cambiare la sua mail, ecc. ma solo la pwd

L'utente non deve poter eliminare il suo account

Nella pagina cambia password prevedere l'inserimento della password attuale


## Funzionalità trasversali
La visibilità colonna di tutte le tabelle, deve lasciare almeno una colonna visibile
