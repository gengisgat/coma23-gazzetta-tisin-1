# Gazzetta del Tisin

## Installazione software per far girare l'applicativo
Per installare il codice in locale segui queste istruzioni:

- Per windows: 
    - scarica [Laragon Full](https://laragon.org/download/index.html) e installa l'applicazione. 
    - Fai partire l'applicativo, avvia i servizi 
    - Fai un click con il tasto destro del mouse sullo sfondo della finestra, scegli la voce "creazione veloce", clicca su "progetto Laravel"
    - dai un nome al progetto senza spazi e scrivi tutto in minuscolo

- Per il Mac: segui le istruzioni per installare [Herd](https://herd.laravel.com/).



## Instruzione per installare l'applicativo

- esegui in una cartella il comando ``` git clone https://gitlab.com/gengisgat/coma23-gazzetta-tisin-1.git ```
- il contenuto della cartella spostalo nella cartella di progetto creata con il tool Laragon o Herd

Apri una console da linea di comando (per Laragon usa quella insterna all'applicativo) accedi alla cartella del progetto ed esegui i seguenti comandi:
 
```
 composer install 
 php artisan migrate --seed 
 php artisan key:generate 
 php artisan storage:link  
```

Accedi alla pagina del progetto con le seguenti credenziali:
Username: admin@admin.com
Password: password