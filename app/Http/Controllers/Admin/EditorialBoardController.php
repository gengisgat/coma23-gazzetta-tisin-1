<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEditorialBoardRequest;
use App\Http\Requests\StoreEditorialBoardRequest;
use App\Http\Requests\UpdateEditorialBoardRequest;
use App\Models\Area;
use App\Models\EditorialBoard;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EditorialBoardController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('editorial_board_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $editorialBoards = EditorialBoard::with(['areas'])->get();

        return view('admin.editorialBoards.index', compact('editorialBoards'));
    }

    public function create()
    {
        abort_if(Gate::denies('editorial_board_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $areas = Area::pluck('name', 'id');

        return view('admin.editorialBoards.create', compact('areas'));
    }

    public function store(StoreEditorialBoardRequest $request)
    {
        $editorialBoard = EditorialBoard::create($request->all());
        $editorialBoard->areas()->sync($request->input('areas', []));

        return redirect()->route('admin.editorial-boards.index');
    }

    public function edit(EditorialBoard $editorialBoard)
    {
        abort_if(Gate::denies('editorial_board_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $areas = Area::pluck('name', 'id');

        $editorialBoard->load('areas');

        return view('admin.editorialBoards.edit', compact('areas', 'editorialBoard'));
    }

    public function update(UpdateEditorialBoardRequest $request, EditorialBoard $editorialBoard)
    {
        $editorialBoard->update($request->all());
        $editorialBoard->areas()->sync($request->input('areas', []));

        return redirect()->route('admin.editorial-boards.index');
    }

    public function show(EditorialBoard $editorialBoard)
    {
        abort_if(Gate::denies('editorial_board_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $editorialBoard->load('areas', 'editorialBoardUsers');

        return view('admin.editorialBoards.show', compact('editorialBoard'));
    }

    public function destroy(EditorialBoard $editorialBoard)
    {
        abort_if(Gate::denies('editorial_board_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $editorialBoard->delete();

        return back();
    }

    public function massDestroy(MassDestroyEditorialBoardRequest $request)
    {
        $editorialBoards = EditorialBoard::find(request('ids'));

        foreach ($editorialBoards as $editorialBoard) {
            $editorialBoard->delete();
        }

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
