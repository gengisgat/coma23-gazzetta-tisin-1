<?php

namespace App\Http\Requests;

use App\Models\EditorialBoard;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreEditorialBoardRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('editorial_board_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'min:2',
                'required',
                'unique:editorial_boards',
            ],
            'areas.*' => [
                'integer',
            ],
            'areas' => [
                'array',
            ],
        ];
    }
}
