<?php

namespace App\Http\Requests;

use App\Models\EditorialBoard;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyEditorialBoardRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('editorial_board_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:editorial_boards,id',
        ];
    }
}
