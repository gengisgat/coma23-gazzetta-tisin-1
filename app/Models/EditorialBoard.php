<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EditorialBoard extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'editorial_boards';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function editorialBoardUsers()
    {
        return $this->hasMany(User::class, 'editorial_board_id', 'id');
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class);
    }
}
