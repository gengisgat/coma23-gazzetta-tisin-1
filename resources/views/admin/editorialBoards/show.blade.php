@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.editorialBoard.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.editorial-boards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.editorialBoard.fields.id') }}
                        </th>
                        <td>
                            {{ $editorialBoard->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.editorialBoard.fields.name') }}
                        </th>
                        <td>
                            {{ $editorialBoard->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.editorialBoard.fields.area') }}
                        </th>
                        <td>
                            @foreach($editorialBoard->areas as $key => $area)
                                <span class="label label-info">{{ $area->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.editorial-boards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#editorial_board_users" role="tab" data-toggle="tab">
                {{ trans('cruds.user.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="editorial_board_users">
            @includeIf('admin.editorialBoards.relationships.editorialBoardUsers', ['users' => $editorialBoard->editorialBoardUsers])
        </div>
    </div>
</div>

@endsection