@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.editorialBoard.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.editorial-boards.update", [$editorialBoard->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.editorialBoard.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $editorialBoard->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.editorialBoard.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="areas">{{ trans('cruds.editorialBoard.fields.area') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('areas') ? 'is-invalid' : '' }}" name="areas[]" id="areas" multiple>
                    @foreach($areas as $id => $area)
                        <option value="{{ $id }}" {{ (in_array($id, old('areas', [])) || $editorialBoard->areas->contains($id)) ? 'selected' : '' }}>{{ $area }}</option>
                    @endforeach
                </select>
                @if($errors->has('areas'))
                    <div class="invalid-feedback">
                        {{ $errors->first('areas') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.editorialBoard.fields.area_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection